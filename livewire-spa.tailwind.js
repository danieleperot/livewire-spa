const { fontFamily } = require('tailwindcss/defaultTheme')

module.exports = {
    colors: {
        black: '#222222'
    },

    fontFamily: {
        sans: [
            'Nunito',
            ...fontFamily.sans
        ]
    }
}
