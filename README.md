# Livewire SPA

Piccolo progetto in Laravel per dimostrare che un applicativo costruito con
Liveware si può usare come una SPA senza dover installare Turbolink.

Altri motivi nei [perché](#perche).

## Installazione
> Questo repository usa Laravel 8.x, che richiede almeno PHP 7.3!

1. Clona il repository
2. Crea il file `database.sqlite` (viene usato sqlite perché è facile!)
3. Rinomina il file `.env.local` in `.env`
4. `composer install` e `yarn prod`
5. `php artisan migrate`
6. `php artisan serve`!
7. Visualizza [http://livewire-spa.localhost:8000](http://livewire-spa.localhost:8000) nel browser

## Perché?
Volevo sistemare questi aspetti di Livewire che non mi piacciono:

1. La logica va fuori dal controller e direttamente nella classe del componente
2. Non è possibile avere una SPA

Non volevo usare Inertia perché:

1. Il bundle JS diventa sempre più pesante ad ogni pagina aggiunta
2. Non esiste un SSR per Inertia

Con questo repository si ottengono questi risultati:

1. La logica è nei controller come per i componenti di Inertia (qui un piccolo esempio):
    ```php
    use Nodopiano\SpaRouter\SpaRouter;

    class WelcomeController extends Controller
    {
        public function index()
        {
            return SpaRouter::render('welcome', ['name' => 'Livewire']);
        }
    }
    ```
2. I template sono scritti in blade e sfruttano livewire (che devo testare ancora di più)
3. Il primo render è server-side che potrebbe essere utile
4. Si comporta come una SPA senza usare Turbolink
