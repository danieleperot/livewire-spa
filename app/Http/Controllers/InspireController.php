<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Nodopiano\SpaRouter\SpaRouter;

class InspireController extends Controller
{
    public function index()
    {
        return SpaRouter::render('inspire', ['name' => 'Livewire']);
    }
}
