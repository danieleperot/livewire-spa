<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Nodopiano\SpaRouter\SpaRouter;

class WelcomeController extends Controller
{
    public function index()
    {
        return SpaRouter::render('welcome', ['name' => 'Livewire']);
    }
}
