<?php

namespace App\Http\Livewire;

use Illuminate\Foundation\Inspiring;
use Livewire\Component;

class Inspire extends Component
{
    public $quote;

    public function mount()
    {
        $this->generateQuote();
    }

    public function generateQuote()
    {
        $this->quote = Inspiring::quote();
    }

    public function render()
    {
        return view('livewire.inspire');
    }
}
