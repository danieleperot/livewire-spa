<?php

namespace Nodopiano\SpaRouter\Livewire;

use Livewire\Component;

class Router extends Component
{
    public $component;
    public $variables;

    public function changePage($pageData)
    {
        if (is_array($pageData)) {
            $this->component = $pageData['component'];
            $this->variables = $pageData['variables'];
        }
    }

    public function mount($component, $variables)
    {
        $this->component = $component;
        $this->variables = $variables;
    }

    public function render()
    {
        return <<<'blade'
        <div class="livewire-router">
            @livewire($component, $variables, key(json_encode($variables)))

            <script>
                document.addEventListener('livewire:load', function () { window.routerComponent = @this; });
            </script>
        </div>
        blade;
    }
}
