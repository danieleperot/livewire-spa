<?php

namespace Nodopiano\SpaRouter\Providers;

use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Nodopiano\SpaRouter\Livewire\Router;

class SpaRouterServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Livewire::component('spa-router', Router::class);
    }
}
