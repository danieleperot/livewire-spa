<?php

namespace Nodopiano\SpaRouter;

use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Container\BindingResolutionException;

class SpaRouter
{
    /**
     * @var string
     */
    protected $view;

    /**
     * @var array
     */
    protected $variables;

    /**
     * @param string $view
     * @param array $variables
     * @return void
     */
    public function __construct($view, $variables)
    {
        $this->view = $view;
        $this->variables = $variables;
    }

    /**
     * @param string $view
     * @param array $variables
     * @return View|Factory|JsonResponse
     * @throws BindingResolutionException
     */
    public static function render($view, $variables = [])
    {
        return (new static($view, $variables))->response();
    }

    /**
     * @return View|Factory|JsonResponse
     * @throws BindingResolutionException
     */
    public function response()
    {
        return $this->isSpaRouterRequest()
            ? response()->json($this->prepareParameters())
            : view($this->getBladeViewName(), $this->prepareParameters());
    }

    /**
     * @return bool
     * @throws BindingResolutionException
     */
    protected function isSpaRouterRequest()
    {
        return request()->hasHeader('X-Spa-Link');
    }

    /**
     * @return array
     */
    protected function prepareParameters()
    {
        return [
            'component' => $this->view,
            'variables' => array_merge($this->variables, [
                '__component' => $this->view,
                '__time' => time()
            ])
        ];
    }

    /**
     * @return string
     */
    protected function getBladeViewName()
    {
        return 'app';
    }
}
