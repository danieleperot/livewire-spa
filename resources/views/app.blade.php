<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-full">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @livewireStyles
</head>
<body class="h-full flex flex-col">
    <div class="h-full flex flex-grow flex-col bg-gray-300 px-12 py-8">
        <header class="mb-4">
            <h1 class="text-3xl font-bold text-gray-700">Livewire SPA</h1>
        </header>
        <main class="flex-grow h-full bg-white shadow-xl p-8 rounded-lg">
            <livewire:spa-router :variables="$variables" :component="$component">
        </main>
    </div>

    @livewireScripts
</body>
</html>
