<div>
    <div class="flex items-center">
        <div>{{ $quote }}</div>

        <button wire:click="generateQuote" class="shadow-md inline-block bg-gray-100 px-4 py-1 text-sm rounded ml-4">
            Genera frase
        </button>
    </div>

    <a spa:link href="{{ route('welcome') }}" class="shadow-md inline-block bg-gray-100 px-4 py-1 text-sm rounded mt-8">
        Vai alla home
    </a>
</div>
