<div>
    <div class="font-bold">
        Scrivi il tuo nome:
    </div>

    <div class="my-4">
        <input wire:model="name" class="border-2 border-gray-200 px-3 py-1 text-sm rounded">
    </div>

    <div>
        Ciao, {{ $name }}!
    </div>

    <a spa:link href="{{ route('inspire') }}" class="shadow-md inline-block bg-gray-100 px-4 py-1 text-sm rounded mt-8">
        Vai a Ispirazioni
    </a>
</div>
