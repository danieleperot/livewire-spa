import axios from 'axios'
import 'typeface-nunito'

window.axios = axios

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
