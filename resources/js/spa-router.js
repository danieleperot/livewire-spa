import Axios from 'axios'

const findLink = element => Object.values(element.attributes).find(attribute => attribute.name === 'href').value

const npLink = Axios.create({ headers: { 'X-Spa-Link': true } })

export default class SpaRouter {
  constructor() {
    document.addEventListener('livewire:load', () => {
      this.loadRouter()
      Livewire.hook('component.initialized', () => this.loadRouter())
    })
  }

  loadRouter() {
    document.querySelectorAll('[spa\\:link]').forEach((element) => {
      element.addEventListener('click', (event) => {
        event.preventDefault()
        let link = findLink(element)
        npLink.get(link).then(({ data }) => {
          window.routerComponent.changePage(data).then(() => window.history.pushState({}, '', link))
        })
      })
    })
  }
}
